import { Component, OnInit } from '@angular/core';
const firebase = require("nativescript-plugin-firebase");

@Component({
	moduleId: module.id,
	selector: 'login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

	constructor() { }

	ngOnInit() { 
		firebase.init().then(
			instance => {
			  console.log("firebase.init done");
			},
			error => {
			  console.log(`firebase.init error: ${error}`);
			}
		  );
	}
}