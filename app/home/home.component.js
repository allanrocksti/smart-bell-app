"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var call_1 = require("~/models/call");
var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
        this.calls = [];
    }
    HomeComponent.prototype.ngOnInit = function () {
        var call1 = new call_1.Call(1, new Date('2018-04-01T00:00:00Z'));
        var call2 = new call_1.Call(2, new Date('2018-04-01T00:00:00Z'));
        var call3 = new call_1.Call(3, new Date('2018-04-01T00:00:00Z'));
        var call4 = new call_1.Call(4, new Date('2018-04-01T00:00:00Z'));
        var call5 = new call_1.Call(5, new Date('2018-04-01T00:00:00Z'));
        this.calls.push(call1);
        this.calls.push(call2);
        this.calls.push(call3);
        this.calls.push(call4);
        this.calls.push(call5);
    };
    HomeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'home',
            templateUrl: './home.component.html',
            styleUrls: ['./home.component.css']
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUVsRCxzQ0FBcUM7QUFTckM7SUFJQztRQUZBLFVBQUssR0FBVyxFQUFFLENBQUM7SUFFSCxDQUFDO0lBRWpCLGdDQUFRLEdBQVI7UUFFQyxJQUFJLEtBQUssR0FBRyxJQUFJLFdBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO1FBQzFELElBQUksS0FBSyxHQUFHLElBQUksV0FBSSxDQUFDLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7UUFDMUQsSUFBSSxLQUFLLEdBQUcsSUFBSSxXQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUMxRCxJQUFJLEtBQUssR0FBRyxJQUFJLFdBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO1FBQzFELElBQUksS0FBSyxHQUFHLElBQUksV0FBSSxDQUFDLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7UUFFMUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQW5CVyxhQUFhO1FBUHpCLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLE1BQU07WUFDaEIsV0FBVyxFQUFFLHVCQUF1QjtZQUNwQyxTQUFTLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztTQUNuQyxDQUFDOztPQUVXLGFBQWEsQ0FxQnpCO0lBQUQsb0JBQUM7Q0FBQSxBQXJCRCxJQXFCQztBQXJCWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IENhbGwgfSBmcm9tICd+L21vZGVscy9jYWxsJztcblxuQENvbXBvbmVudCh7XG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXG5cdHNlbGVjdG9yOiAnaG9tZScsXG5cdHRlbXBsYXRlVXJsOiAnLi9ob21lLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vaG9tZS5jb21wb25lbnQuY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBIb21lQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuXHRjYWxsczogQ2FsbFtdID0gW107XG5cblx0Y29uc3RydWN0b3IoKSB7IH1cblxuXHRuZ09uSW5pdCgpIHsgXG5cblx0XHRsZXQgY2FsbDEgPSBuZXcgQ2FsbCgxLCBuZXcgRGF0ZSgnMjAxOC0wNC0wMVQwMDowMDowMFonKSk7XG5cdFx0bGV0IGNhbGwyID0gbmV3IENhbGwoMiwgbmV3IERhdGUoJzIwMTgtMDQtMDFUMDA6MDA6MDBaJykpO1xuXHRcdGxldCBjYWxsMyA9IG5ldyBDYWxsKDMsIG5ldyBEYXRlKCcyMDE4LTA0LTAxVDAwOjAwOjAwWicpKTtcblx0XHRsZXQgY2FsbDQgPSBuZXcgQ2FsbCg0LCBuZXcgRGF0ZSgnMjAxOC0wNC0wMVQwMDowMDowMFonKSk7XG5cdFx0bGV0IGNhbGw1ID0gbmV3IENhbGwoNSwgbmV3IERhdGUoJzIwMTgtMDQtMDFUMDA6MDA6MDBaJykpO1xuXG5cdFx0dGhpcy5jYWxscy5wdXNoKGNhbGwxKTtcblx0XHR0aGlzLmNhbGxzLnB1c2goY2FsbDIpO1xuXHRcdHRoaXMuY2FsbHMucHVzaChjYWxsMyk7XG5cdFx0dGhpcy5jYWxscy5wdXNoKGNhbGw0KTtcblx0XHR0aGlzLmNhbGxzLnB1c2goY2FsbDUpO1xuXHR9XG5cbn0iXX0=