import { Component, OnInit } from '@angular/core';

import { Call } from '~/models/call';

@Component({
	moduleId: module.id,
	selector: 'home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

	calls: Call[] = [];

	constructor() { }

	ngOnInit() { 

		let call1 = new Call(1, new Date('2018-04-01T00:00:00Z'));
		let call2 = new Call(2, new Date('2018-04-01T00:00:00Z'));
		let call3 = new Call(3, new Date('2018-04-01T00:00:00Z'));
		let call4 = new Call(4, new Date('2018-04-01T00:00:00Z'));
		let call5 = new Call(5, new Date('2018-04-01T00:00:00Z'));

		this.calls.push(call1);
		this.calls.push(call2);
		this.calls.push(call3);
		this.calls.push(call4);
		this.calls.push(call5);
	}

}